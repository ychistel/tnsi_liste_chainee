Les listes chainées
====================

Contenus
-----------

-   Structures de données, interface et implémentation.
-   Liste chainée : structure linéaire.

Capacités attendues
--------------------

-   Spécifier une structure de données par son interface.
-   Distinguer interface et implémentation
-   Écrire plusieurs implémentations d'une même structure de données.
-   Distinguer des structures par le jeu des méthodes qui les caractérisent.
-   Choisir une structure de données adaptées à la situation à modéliser.

.. toctree::
   :maxdepth: 1
   :hidden:
   
   content/liste_chaine.rst
   content/exercice.rst
