def creer_liste():
    """Renvoie un tuple vide"""
    return ...

def est_vide(liste):
    """Renvoie True si le tuple est vide; False si non"""
    ...

def inserer(element, liste):
    """Renvoie la liste chainee avec un nouvel element"""
    ...

def tete(liste):
    """Renvoie la tete de la liste chainee"""
    ...

def queue(liste):
    """Renvoie la queue de la liste chainee"""
    ...
